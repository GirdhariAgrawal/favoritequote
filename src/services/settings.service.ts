export class SettingsService {

  private altBackground = false;

  changeBackground(value: boolean) {
    this.altBackground = value;
    console.log(this.altBackground);
  }

  isAltBackground() {
    return this.altBackground;
  }
}
