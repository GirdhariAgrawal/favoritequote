import {Quote} from "../model/Quote";

export class QuotesService {

  favoritesQuote: Quote[] = [];

  addQuoteToFavorites(quote: Quote) {
    this.favoritesQuote.push(quote);
  }

  removeQuoteFromFavorites(quote: Quote) {
    const position = this.favoritesQuote.findIndex((quoteEl) => {
      return quote.id == quoteEl.id;
    });
    this.favoritesQuote.splice(position, 1);
  }

  getFavoritesQuotes() {
    return this.favoritesQuote.slice();
  }

  isQuoteFavorites(quote: Quote) {
    return this.favoritesQuote.find( (quoteEL) => {
      return quote.id == quoteEL.id;
    });
  }
}
