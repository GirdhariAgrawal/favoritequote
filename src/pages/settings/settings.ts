import {Component} from '@angular/core';
import { Toggle } from "ionic-angular";
import {SettingsService} from "../../services/settings.service";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class Settings {

  constructor(private settingService: SettingsService) {

  }
  onToggle(e: Toggle) {
    this.settingService.changeBackground(e.checked);
  }

  checkAltBackground() {
    return this.settingService.isAltBackground();
  }

}
