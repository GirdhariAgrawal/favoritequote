import {Component} from "@angular/core";
import {FavoritesPage} from "../favorites/favorites";
import {LibraryPage} from "../library/library";

@Component({
  selector: 'page-tabs',
  template: `
    <ion-tabs>
      <ion-tab [root]="favoritesPage" tabTitle="Favroites" tabIcon="star"></ion-tab>
      <ion-tab [root]="library" tabTitle="Libarary" tabIcon="book"></ion-tab>
    </ion-tabs>
  `
})
export class TabsPage {
  favoritesPage = FavoritesPage;
  library = LibraryPage;
  
  constructor() {
	console.log(this.favoritesPage);
  }
}
