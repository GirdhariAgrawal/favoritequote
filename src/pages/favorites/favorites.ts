import { Component } from '@angular/core';
import {Quote} from "../../model/Quote";
import {QuotePage} from "../quote/quote";
import {QuotesService} from "../../services/quotes.service";
import {ModalController} from "ionic-angular";
import {SettingsService} from "../../services/settings.service";

@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {
  quotes: Quote[];

  constructor(private quoteService: QuotesService,
              private modalCtrl: ModalController,
              private settingsService: SettingsService) { }

  ionViewWillEnter() {
    this.quotes = this.quoteService.getFavoritesQuotes();
  }

  onViewQuote(quote: Quote) {
    const modal = this.modalCtrl.create(QuotePage, quote);
    modal.present();
    modal.onDidDismiss((remove) => {
      if(remove) {
        this.removeQuote(quote);
      }
    });
  }

  onRemoveFromFavorites(quote) {
    this.removeQuote(quote);
  }

  removeQuote(quote) {
      this.quoteService.removeQuoteFromFavorites(quote);
      this.quotes = this.quoteService.getFavoritesQuotes();
  }

  getBackground() {
    console.log(this.settingsService.isAltBackground());
    return this.settingsService.isAltBackground() ? 'altQuoteBackground': 'quoteBackground';
  }
}
