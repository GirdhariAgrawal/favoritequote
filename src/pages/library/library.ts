import {Component, OnInit} from '@angular/core';
import {NavController, NavParams} from "ionic-angular";
import {Quote} from "../../model/Quote";
import quotes from "../../data/quotes";
import {QuotesPage} from "../quotes/quotes";

@Component({
  selector: 'page-library',
  templateUrl: 'library.html',
})
export class LibraryPage implements OnInit {
  quoteCollection: { category: string, quotes: Quote[], icon: string}[];

  constructor(private navCtrl: NavController) {}

  goToQuotes(quote: Quote) {
    this.navCtrl.push(QuotesPage, quote);
  }
  ngOnInit() {
    this.quoteCollection = quotes;
  }
}
